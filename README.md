# MkDocs demo

This project shows an MkDocs and Material for MkDocs demo.

**Table of contents:**
- [MkDocs demo](#mkdocs-demo)
  - [Makefile shortcuts](#makefile-shortcuts)
  - [Running locally](#running-locally)
  - [Building for deployment](#building-for-deployment)
    - [GitLab Pages](#gitlab-pages)

## Makefile shortcuts

This project provides a
[Makefile](https://www.gnu.org/software/make/manual/make.html#Introduction)
and underlying [Makefile.venv](https://github.com/sio/Makefile.venv)
with helpful shortcuts for interacting with the demo.

For all instructions in the README, use the provided Makefile. If you
cannot use a Makefile in your Command-Line utility, open the file and
extract the commands from the targets.

## Running locally

Serve the MkDocs project locally and view it on
[http://127.0.0.1:8000/demos/mkdocs-demo/](http://127.0.0.1:8000/demos/mkdocs-demo/):

```sh
make mkdocs-serve
```

## Building for deployment

Generate the project as a static site to the `site` directory:

```sh
make mkdocs-build
```

### GitLab Pages

This website is published on [GitLab
Pages](https://docs.gitlab.com/ee/user/project/pages/). See
`.gitlab-ci.yml` for the CI configuration.

Click to [view this page on GitLab
Pages](https://mah-rye-kuh.gitlab.io/demos/mkdocs-demo/).
