#
# Configuration variables
#

PY=python3.11


#
# Targets
#

.PHONY: mkdocs-serve
mkdocs-serve: venv
	$(VENV)/mkdocs serve

.PHONY: mkdocs-build
mkdocs-build: venv
	$(VENV)/mkdocs build


#
# Include other Makefiles
#

# Source: https://github.com/sio/Makefile.venv/blob/master/Makefile.venv
include Makefile.venv
