# Highlights

Check below for some of my personal MkDocs highlights.

## Plugins, plugins, and more plugins

Did I mention that there are loads of plugins available?

Material comes with many features out of the box; you will only need to activate them. Many of these features come free, but some are paid for. However, you do not need to pay to get a premium experience.

See also:

- [Material plugin overview](https://squidfunk.github.io/mkdocs-material/plugins/)
- [Pymdown Extensions](https://facelessuser.github.io/pymdown-extensions/)
- [MkDocs packages on PyPI](https://pypi.org/search/?q=mkdocs)

## Syntax highlighting

Wrap code in single backticks to show them `inline code`.

Use triple backticks to wrap multiple lines of code, or to create a code block:

```
This is a block of code.
```

For detailed examples, see [SuperFences documentation](https://facelessuser.github.io/pymdown-extensions/extensions/superfences/).

## Diagrams

Generate diagrams using [Mermaid](https://mermaid.js.org/).

Want to explain a complex flow within an application? **Flow charts** have for you covered:

```mermaid
flowchart LR
    do-the-thing[Do the thing] --> ask-thing-succesful{Success?};
    ask-thing-succesful -->|No| try-again[Try again];
    try-again --> do-the-thing
    ask-thing-succesful -->|Yes| profit[Profit!];
```

Remember that Material **does not officially support every diagram
type,** but you can use them. For example, pie charts:

```mermaid
pie title
    "Fuzzy llama":40
    "Funny llama":20
    "Llama llama":10
    "Duck":5
```

For detailed examples, see [Diagrams documentation](https://squidfunk.github.io/mkdocs-material/reference/diagrams/).

## Snippets

Use snippets to load complete files or file sections on a page.
Optionally wrap the output in fences for syntax highlighting.

```python
--8<-- "src/hello_world.py"
```

For detailed examples, see [Snippets documentation](https://facelessuser.github.io/pymdown-extensions/extensions/snippets/).

!!! Warning

    Showing arbitrary bits of code in documentation can lead to security
    incidents. Do not use this feature on user-facing websites!
