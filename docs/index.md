# Home

Do you want to publish (internal) documentation alongside your project?
Try [MkDocs](https://www.mkdocs.org)!

This demo uses [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/) on top of MkDocs
to generate a good-looking documentation website with minimal effort.

!!! Note

    Are you reading this page from the source code? Click to [view this
    page on GitLab
    Pages](https://mah-rye-kuh.gitlab.io/demos/mkdocs-demo/).

## For whom, and why?

While there are many use cases for a static site generator, I
(_Mah-Rye-Kuh_, the creator of this demo) prefer to use the software
like so:

Provide overall documentation of a project written in human language,
geared towards **technical developers and managers,** all searchable by
default!

Leave code-specific documentation with the code, but use MkDocs to
explain application flows and philosophies, conventions, and more.

**Enhance the documentation with graphs, media, and more** features than
you could provide in your code, all the while using a markup format
familiar to developers:
[Markdown](https://daringfireball.net/projects/markdown/).

## Documentation and source code stay together

Let's face it: keeping documentation up-to-date is not a developer's
favorite task if they even remember that the documentation exists.

Host the  `docs/` directory next to the `src/` directory, and MkDocs
provides you with these perks:

- Use your trusted code editor to update the documentation.
- Use Markdown instead of fiddling with a WYSIWYG editor.
- Use Git to manage the files for you.
- Update code and documentation in a single pull request.
- Allow developers to review documentation in the same way they would do
  with code.
- Preview the documentation locally before publication.
- Integrate deployment of the files in your CI flows.
- Publish automatically to [GitHub Pages](https://pages.github.com/) or
  [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Use MkDocs for internal documentation to create practical and technical
documentation away from sales and marketing websites.

## Customize to your preferences

MkDocs and especially _Material for MkDocs_ are highly customizable out
of the box.

Add plugins, change colors, logos, dark and light modes, emoji (:tada:),
and one of my favorites: [Mermaid diagrams](https://mermaid.js.org/).

Use MkDocs inheritance to share and reuse basic configuration. You could
even create a custom Docker image for your organization.

!!! Tip

    Dive into the Material theme documentation to see what you need;
    they also cover many of the MkDocs core basics.

<!-- Abbreviations: -->
*[CI]: Continuous Integration
*[WYSIWYG]: What You See Is What You Get
